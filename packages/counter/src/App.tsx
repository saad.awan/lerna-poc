import React, { useState } from "react";
import "./App.css";
import { Button } from "@monorepo/timer";

function App() {
  const [counter, setCounter] = useState(0);

  return (
    <div className="App">
      <header className="App-header">
        <Button onClick={() => setCounter((prev) => ++prev)} name="Click Me" />
        <span>Your count is {counter}</span>
      </header>
    </div>
  );
}

export default App;
