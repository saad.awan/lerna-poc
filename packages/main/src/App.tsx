import { ElapsedTime } from "@monorepo/timer";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ElapsedTime />
      </header>
    </div>
  );
}

export default App;
