import React, { useState, useEffect } from "react";
import { useElapsedTime } from "use-elapsed-time";

import "../App.css";

function ElapsedTime() {
  const [isPlaying, setIsPlaying] = useState(false);
  const { elapsedTime } = useElapsedTime({ isPlaying });

  useEffect(() => {
    document.addEventListener("keyup", () => {
      setIsPlaying((prevIsPlaying) => !prevIsPlaying);
    });
  }, []);

  return (
    <div className="watch">
      <h1>Elapsed Time</h1>
      <p>Press any key to play/pause time</p>
      <div style={{ fontSize: 56 }}>{elapsedTime.toFixed(2)}</div>
    </div>
  );
}

export default ElapsedTime;
