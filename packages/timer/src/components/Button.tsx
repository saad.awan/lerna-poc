import * as React from "react";
import "../App.css";

interface ButtonProps {
  onClick: () => void;
  name: string;
}

const Button = (props: ButtonProps) => {
  const { onClick, name } = props;
  return (
    <button className="watch" onClick={onClick}>
      {name}
    </button>
  );
};

export default Button;
